import { defineStore } from 'pinia'
import { load_trace_wasm } from 'sqtt'
import { markRaw } from 'vue'

export const useStore = defineStore('main', {
  state: () => {
    return {
      loadedProfile: <any>null,
      scrollIndex: 0,
    }
  },
  actions: {
    load (buf: ArrayBuffer) {
      this.loadedProfile = markRaw(load_trace_wasm(new Uint8Array(buf)))
    },
    scrollEvents (idx: number) {
      this.scrollIndex = idx
    }
  },
})
