Right now, these register mappings are generated from RADV register JSON files with:

```shell
jq '{ enums: .enums | map_values(.entries | map({(.value|tostring) : .name}) | add), regs: .register_mappings | map({(.map.at|tostring) : .name}) | add }'
```
