// Given an array where predicate would return [false, ..., false, true, ..., true], find the first element where
// predicate returns true
function binarySearch<T> (a: ArrayLike<T>, predicate: (x: T) => boolean): number {
  let len = a.length
  let left = 0
  let right = len
  while (left < right) {
    const mid = left + Math.floor(len / 2)
    if (!predicate(a[mid])) {
      left = mid + 1
    } else {
      right = mid
    }
    len = right - left
  }
  return left
}

export function lowerbound<T> (a: ArrayLike<T>, value: T): number {
  return binarySearch(a, (x) => x >= value)
}
