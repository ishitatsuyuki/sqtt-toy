import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createPinia } from 'pinia'
import 'vue-virtual-scroller/dist/vue-virtual-scroller.css'
// @ts-ignore
import VueVirtualScroller from 'vue-virtual-scroller'

import {
  allComponents, baseLayerLuminance,
  provideFASTDesignSystem, StandardLuminance,
} from '@microsoft/fast-components'

provideFASTDesignSystem().register(allComponents);
baseLayerLuminance.withDefault(StandardLuminance.LightMode);

const app = createApp(App)

app.use(createPinia())
app.use(VueVirtualScroller)
app.mount('#app')
