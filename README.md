# sqtt-toy

A reverse engineered parser for RGP traces.

This project is a hybrid of Vue.js for visualization/frontend and Rust (WASM) for performance-sensitive parsing.

The implementation doesn't try to favor a particular browser, but right now the WASM code runs much faster in Chromium.

Install [pnpm](https://pnpm.io/installation).

Install NPM dependencies:

```shell
pnpm install
```

Run frontend development server:

```shell
pnpm run dev
```

**Scripts are not guaranteed to typecheck right now (type errors are ignored in dev mode).**

Build WASM code:

```shell
cd rust
wasm-pack build --features console_error_panic_hook
```

You might need to install a git version of wasm-bindgen into your $PATH.
