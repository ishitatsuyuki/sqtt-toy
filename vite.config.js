import vue from '@vitejs/plugin-vue'
import wasm from 'vite-plugin-wasm'

export default {
  plugins: [wasm(), vue()],
}
