use std::io::{BufRead, Cursor, Seek, SeekFrom};

use anyhow::Result;
use js_sys::Reflect;
use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

use crate::metadata::{parse_entry_header, parse_header, ENTRY_HEADER_SIZE};
use crate::sqtt::{parse_sqtt, SqttData};

mod metadata;
mod sqtt;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn load_trace_wasm(buf: &[u8]) -> std::result::Result<JsValue, JsError> {
    let profile = load_trace(buf).map_err(|e| JsError::new(&e.to_string()))?;
    let mut ret = JsValue::from_serde(&profile)?;
    let chunks = Reflect::get(&ret, &JsValue::from_str("sqttChunks")).unwrap();
    for (i, sqtt) in profile.sqtt_chunks.iter().enumerate() {
        let obj = Reflect::get(&chunks, &JsValue::from_str(&i.to_string())).unwrap();
        sqtt.fixup_js_object(&obj)?;
    }
    Ok(ret)
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RgpProfile {
    pub sqtt_chunks: Vec<SqttData>,
}

pub fn load_trace(buf: &[u8]) -> Result<RgpProfile> {
    let hdr = parse_header(buf)?;
    let mut sqtt_chunks = vec![];
    let mut offset = hdr.chunk_offset as usize;
    while offset < buf.len() {
        let entry = parse_entry_header(&buf[offset..])?;
        let len = (entry.size as usize) - ENTRY_HEADER_SIZE;
        if (entry.chunk_id & 255) == 2 {
            let start = offset + ENTRY_HEADER_SIZE + 8;
            let sqtt_data = &buf[start..start + len];
            sqtt_chunks.push(parse_sqtt(sqtt_data)?);
        }
        offset += entry.size as usize;
    }
    Ok(RgpProfile { sqtt_chunks })
}
