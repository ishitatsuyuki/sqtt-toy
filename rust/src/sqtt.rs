use std::convert::{TryFrom, TryInto};
use std::io::Cursor;
use std::{mem, ptr};

use anyhow::{anyhow, bail, Result};
use heck::ToLowerCamelCase;
use js_sys::{BigUint64Array, Object, Reflect, Uint16Array, Uint32Array, Uint8Array};
use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

trait ToTypedArray {
    type TypedArray;
    fn to_typed_array(&self) -> Self::TypedArray;
}

impl ToTypedArray for Vec<u8> {
    type TypedArray = Uint8Array;

    fn to_typed_array(&self) -> Self::TypedArray {
        Uint8Array::from(&**self)
    }
}

impl ToTypedArray for Vec<u16> {
    type TypedArray = Uint16Array;

    fn to_typed_array(&self) -> Self::TypedArray {
        Uint16Array::from(&**self)
    }
}

impl ToTypedArray for Vec<u32> {
    type TypedArray = Uint32Array;

    fn to_typed_array(&self) -> Self::TypedArray {
        Uint32Array::from(&**self)
    }
}

impl ToTypedArray for Vec<u64> {
    type TypedArray = BigUint64Array;

    fn to_typed_array(&self) -> Self::TypedArray {
        BigUint64Array::from(&**self)
    }
}

macro_rules! gen_parser {
    (
        $(packet $pkt:ident {
            $([$top:literal:$bottom:literal] $field:ident: $ty:ty,)+
        })+
    ) => {
        $(
            #[derive(Default)]
            pub struct $pkt {
                pub seq: Vec<u32>,
                $(pub $field: Vec<$ty>),+
            }

            impl $pkt {
                fn parse(&mut self, i: &[u8], halfbyte_offset: usize, seq: u32) -> Result<()> {
                    $(let $field = read_bits(i, halfbyte_offset + 1, $bottom - 4, $top + 1 - $bottom)? as $ty;)+
                    $(self.$field.push($field);)+
                    self.seq.push(seq);
                    Ok(())
                }

                fn to_js_object(&self) -> std::result::Result<Object, JsValue> {
                    let obj = Object::new();
                    Reflect::set(
                        &obj,
                        &JsValue::from_str("seq"),
                        &self.seq.to_typed_array().into(),
                    );
                    $(Reflect::set(
                        &obj,
                        &JsValue::from_str(&stringify!($field).to_lower_camel_case()),
                        &self.$field.to_typed_array().into(),
                    )?;)+
                    Ok(obj)
                }
            }
        )+
    };
}

gen_parser! {
    packet Timestamp {
        [15:14] ty: u8,
        // Data says it's at least 40 bits, but it hasn't been tested for longer uptime
        // We also don't know about its wraparound behavior
        [63:16] timestamp_value: u64,
    }

    packet Initiator {
        [15:14] a0: u8,
        [17:16] a1: u8,
        [19:18] initiator_type: u8,
        [52:20] val: u32,
        [46:44] context: u8,
    }

    packet RegWrites {
        [15:15] is_write: u8,
        [31:16] reg: u16,
        [63:32] val: u32,
    }

    packet WaveStart {
        [ 7: 7] a0: u8,
        [ 9: 8] a1: u8,
        [12:10] a2: u8,
        [17:13] a3: u8,
        [21:18] stage: u8,
        [31:25] threads: u8,
    }

    packet WaveAllocEnd {
        [ 4: 4] is_end: u8,
        [ 8: 8] a0: u8,
        [10: 9] a1: u8,
        [13:11] a2: u8,
        [19:15] a3: u8,
    }

    packet EventA {
        [11:11] b0: u8,
        [13:12] selector: u8,
        [17:14] stage: u8,
        [23:18] a0: u8,
    }

    packet EventB {
        [11:11] b0: u8,
        [13:12] selector: u8,
        [17:14] stage: u8,
        [19:18] a0: u8,
        [31:20] a1: u8,
    }

    packet Pkt2 {
        [6:4] dt: u8,
    }

    packet Pkt9 {
        [6:4] dt: u8,
    }

    packet PktF {
        [6:4] dt: u8,
    }
}

#[derive(Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SqttData {
    #[serde(skip)]
    pub timestamps: Timestamp,
    #[serde(skip)]
    pub initiators: Initiator,
    #[serde(skip)]
    pub reg_writes: RegWrites,
    #[serde(skip)]
    pub wave_s: WaveStart,
    #[serde(skip)]
    pub wave_ae: WaveAllocEnd,
    #[serde(skip)]
    pub event_a: EventA,
    #[serde(skip)]
    pub event_b: EventB,
}

impl SqttData {
    pub fn fixup_js_object(&self, obj: &JsValue) -> std::result::Result<(), JsError> {
        Reflect::set(
            obj,
            &JsValue::from_str("timestamps"),
            &self.timestamps.to_js_object().unwrap(),
        )
        .unwrap();
        Reflect::set(
            obj,
            &JsValue::from_str("initiators"),
            &self.initiators.to_js_object().unwrap(),
        )
        .unwrap();
        Reflect::set(
            obj,
            &JsValue::from_str("regWrites"),
            &self.reg_writes.to_js_object().unwrap(),
        )
        .unwrap();
        Reflect::set(
            obj,
            &JsValue::from_str("waveAe"),
            &self.wave_ae.to_js_object().unwrap(),
        )
        .unwrap();
        Reflect::set(
            obj,
            &JsValue::from_str("waveS"),
            &self.wave_s.to_js_object().unwrap(),
        )
        .unwrap();
        Reflect::set(
            obj,
            &JsValue::from_str("eventA"),
            &self.event_a.to_js_object().unwrap(),
        )
        .unwrap();
        Reflect::set(
            obj,
            &JsValue::from_str("eventB"),
            &self.event_b.to_js_object().unwrap(),
        )
        .unwrap();
        Ok(())
    }
}

fn read_bits(i: &[u8], halfbyte_offset: usize, start: usize, width: usize) -> Result<u64> {
    if halfbyte_offset / 2 + 8 >= i.len() {
        bail!("TODO: handle the last packets of the stream");
    }
    // FIXME: this can fail on the last packets of the stream
    // TODO: care about endianness
    let qword = unsafe { ptr::read_unaligned(i.as_ptr().add(halfbyte_offset / 2) as *const u64) };
    Ok(if halfbyte_offset % 2 == 1 {
        qword >> (start + 4)
    } else {
        qword >> start
    } & ((1 << width) - 1))
}

pub fn parse_sqtt(i: &[u8]) -> Result<SqttData> {
    let halfword = |idx| {
        let byte = i[idx / 2];
        if idx % 2 == 0 {
            byte & 0xf
        } else {
            byte >> 4
        }
    };
    let mut cursor = 0;
    let mut ret = SqttData::default();
    // Sequence ID for event ordering information. Probably can be replaced with timestamp once we get to parse it.
    let mut seq = 0;
    while cursor < i.len() * 2 {
        let mut parse_item = || -> Result<()> {
            let selector = halfword(cursor);
            // Note: These values are for Navi 10, check RGP assembly for branching on other targets
            let advance = match selector {
                0 => 1,
                1 => match halfword(cursor + 1) {
                    // Opcode 6/14 use full 4 bits, the other just use 3 bits and the upper 1 is for data
                    0 | 3 | 8 | 11 => 16,
                    1 | 2 | 7 | 9 | 10 | 15 => 16,
                    4 | 12 => 24,
                    5 | 13 | 6 => 6,
                    14 => 8,
                    _ => unimplemented!(),
                },
                // Opcode 2/3 use only lower 3 bits for opcode and upper 1 for data
                2 | 10 | 5 => 5,
                3 | 11 | 13 => 3,
                // Change this to 6 for RDNA2
                4 => 7,
                6 => {
                    if halfword(cursor + 1) & 1 != 0 {
                        7
                    } else {
                        13
                    }
                }
                8 | 14 | 15 => 2,
                9 => 16,
                12 => 8,
                _ => unreachable!(),
            };
            match selector {
                1 => match halfword(cursor + 1) {
                    0 | 8 => {
                        ret.timestamps.parse(i, cursor, seq)?;
                    }
                    7 | 15 => {
                        ret.initiators.parse(i, cursor, seq)?;
                    }
                    6 => {
                        ret.event_a.parse(i, cursor, seq)?;
                    }
                    14 => {
                        ret.event_b.parse(i, cursor, seq)?;
                    }
                    _ => {}
                },
                5 => {
                    ret.wave_ae.parse(i, cursor, seq)?;
                }
                9 => {
                    ret.reg_writes.parse(i, cursor, seq)?;
                }
                12 => {
                    ret.wave_s.parse(i, cursor, seq)?;
                }
                _ => {}
            }
            cursor += advance;
            Ok(())
        };
        if let Err(_) = parse_item() {
            break;
        }
        seq += 1;
    }
    Ok(ret)
}
